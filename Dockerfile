FROM mysql:5.6

MAINTAINER Juan Rojas <jnm.ronquillo@gmail.com>

COPY 01.-Scripts.sql /docker-entrypoint-initdb.d
